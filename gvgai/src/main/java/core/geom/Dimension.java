package core.geom;

import java.io.Serializable;

/**
 * A class emulating the awt dimension class.
 * 
 * This class is no
 */
public class Dimension implements Serializable {
	public int width;
	public int height;
	
	public Dimension() {
		this.height = 0;
		this.width = 0;
	}
	
	public Dimension(int w, int h) {
		this.width = w;
		this.height = h;
	}

	public Dimension(Dimension d) {
		this.width = d.width;
		this.height = d.height;
	}
	
}
