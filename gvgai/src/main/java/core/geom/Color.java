package core.geom;

import java.io.Serializable;

/**
 * Created by webpigeon on 17/04/16.
 */
public class Color implements Serializable {

    public int r;
    public int g;
    public int b;

    public Color(int r, int g, int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public String toString() {
        return String.format("Colour[%d, %d, %d]", r,g,b);
    }
}
