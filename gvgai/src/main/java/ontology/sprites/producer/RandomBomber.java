package ontology.sprites.producer;

import core.VGDLSprite;
import core.content.SpriteContent;
import core.game.Game;
import core.geom.Dimension;
import ontology.Types;
import tools.Utils;
import tools.Vector2d;

/**
 * Created with IntelliJ IDEA.
 * User: Diego
 * Date: 21/10/13
 * Time: 18:26
 * This is a Java port from Tom Schaul's VGDL - https://github.com/schaul/py-vgdl
 */
public class RandomBomber extends SpawnPoint
{
    public RandomBomber(){}

    public RandomBomber(Vector2d position, Dimension size, SpriteContent cnt)
    {
        //Init the sprite
        this.init(position, size);

        //Specific class default parameter values.
        loadDefaults();

        //Parse the arguments.
        this.parseParameters(cnt);
    }

    @Override
	protected void loadDefaults()
    {
        super.loadDefaults();
        color = Types.ORANGE;
        is_static = false;
        is_oriented = true;
        orientation = Types.RIGHT.copy();
        is_npc = true;
        is_stochastic = true;
        speed = 1.0;
    }

    @Override
	public void update(Game game)
    {
        Vector2d act = (Vector2d) Utils.choice(Types.BASEDIRS, game.getRandomGenerator());
        this.physics.activeMovement(this, act, this.speed);
        super.update(game);
    }

    @Override
	public VGDLSprite copy()
    {
        RandomBomber newSprite = new RandomBomber();
        this.copyTo(newSprite);
        return newSprite;
    }

    @Override
	public void copyTo(VGDLSprite target)
    {
        RandomBomber targetSprite = (RandomBomber) target;
        super.copyTo(targetSprite);
    }
}
