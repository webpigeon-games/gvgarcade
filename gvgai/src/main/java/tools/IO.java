package tools;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Diego
 * Date: 04/10/13
 * Time: 16:56
 * This is a Java port from Tom Schaul's VGDL - https://github.com/schaul/py-vgdl
 */
public class IO
{
    /**
     * Default constructor
     */
    public IO(){}

    /**
     * Reads a file and returns its content as a String[]
     * @param filename file to read
     * @return file content as String[], one line per element
     */
    public String[] readFile(String filename)
    {
    	try {
	    	BufferedReader in = new BufferedReader(new FileReader(filename));
	    	return readFile(in);
    	} catch (FileNotFoundException ex) {
            System.out.println("Error reading file : " + ex.toString());
            ex.printStackTrace();
    		return null;
    	}
    }
    
    public String[] readResource(String filename) throws FileNotFoundException, UnsupportedEncodingException {
    	InputStream is = IO.class.getClassLoader().getResourceAsStream(filename);
    	if (is == null) {
    		throw new FileNotFoundException(filename);
    	}
    	BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
    	return readFile(br);
    }
    
    public String[] readFile(BufferedReader in) {
        ArrayList<String> lines = new ArrayList<String>();
        try{
            String line = null;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
            in.close();
        }catch(IOException e)
        {
            System.out.println("Error reading file : " + e.toString());
            e.printStackTrace();
            return null;
        }
        return lines.toArray(new String[lines.size()]);
    }
}
