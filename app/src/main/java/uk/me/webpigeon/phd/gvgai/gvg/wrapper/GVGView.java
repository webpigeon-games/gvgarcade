package uk.me.webpigeon.phd.gvgai.gvg.wrapper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import core.VGDLSprite;
import core.geom.Rectangle;
import ontology.Types;
import tools.Vector2d;
import uk.me.webpigeon.phd.R;
import uk.me.webpigeon.phd.gvgai.gvg.ControlScheme;
import uk.me.webpigeon.phd.gvgai.gvg.GameActivity;
import uk.me.webpigeon.phd.gvgai.gvg.GameState;
import uk.me.webpigeon.phd.gvgai.gvg.mockups.HotzoneControlScheme;

/**
 * Android version of the GVG viewer class
 *
 * Drawing code based on the VGDLSprite code, using android functions rather than the Java2D ones.
 */
public class GVGView extends View implements View.OnTouchListener {
    private Paint background;
    private Paint test;

    private GameActivity activity;
    private ControlScheme controls;

    private Map<String, Bitmap> sprites;

    public GVGView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.activity = (GameActivity)context;
        init();
    }

    public GVGView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (GameActivity)context;
        init();
    }

    public GVGView(Context context) {
        super(context);
        this.activity = (GameActivity)context;
        init();
    }

    private void init() {
        sprites = new HashMap<String, Bitmap>();

        background = new Paint();
        background.setColor(Color.BLACK);
        background.setStyle(Paint.Style.FILL);

        test = new Paint();
        test.setColor(Color.WHITE);
        test.setStyle(Paint.Style.FILL);

        this.setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        //check controls have been initialised
        if (controls == null) {
            controls = new HotzoneControlScheme();
            controls.init(canvas.getWidth(), canvas.getHeight());
        }

        render(canvas);
    }

    public void render(Canvas c) {
        //render a static background
        c.drawRect(0,0,getWidth(), getHeight(), background);

        GameState state = activity.getState();
        if (state == null) {
            return;
        }

        float gridSize = getResources().getDimensionPixelSize(R.dimen.gridSize);

        double width = state.getWidth();
        double height = state.getHeight();

        double scaleX = getWidth() / width;
        double scaleY = getHeight() / height;

        for (VGDLSprite sprite : state.getSprites()) {
            if (sprite.invisible) {
                continue;
            }

            Rectangle r = new Rectangle(sprite.rect);

            if (sprite.image != null) {
                drawImage(c, state, sprite, r, scaleX, scaleY);
            } else {
                drawQuad(c, state, sprite, r, scaleX, scaleY);
            }

            if (!sprite.resources.isEmpty()) {
                drawResources(c, state, sprite, r, scaleX, scaleY);
            }

            if (sprite.healthPoints > 0) {
                //TODO healthbar
            }

            if (sprite.is_oriented) {
                //TODO draw Orientated
            }

        }
    }

    private void setColor(Paint p, core.geom.Color color) {
        int colour = Color.rgb(color.r, color.g, color.b);
        p.setColor(colour);
    }

    private void drawScaled(Canvas c, Paint p, Rectangle r, double scaleX, double scaleY) {
        float x = (float)(r.x * scaleX);
        float y = (float)(r.y * scaleY);
        float bx = (float)(x + (r.width * scaleX));
        float by = (float)(y + (r.height * scaleY));
        c.drawRect(x, y, bx, by, test);
    }

    private void drawResources(Canvas c, GameState game, VGDLSprite s, Rectangle r, double scaleX, double scaleY) {
        double barHeight = r.getHeight() / 3.5f / 3.0f;
        double offset = r.getMinY() + 2 * r.height / 3.0f;

        for (Map.Entry<Integer, Integer> entry : s.resources.entrySet()) {
            int resType = entry.getKey();
            int resValue = entry.getValue();

            if (resType > -1) {
                double wiggle = r.width / 10.0f;
                double prop = Math.max(0, Math.min(1, resValue / (double) (game.getResourceLimit(resType))));

                Rectangle filled = new Rectangle((int) (r.x + wiggle / 2), (int) offset, (int) (prop * (r.width - wiggle)), (int) barHeight);
                Rectangle rest = new Rectangle((int) (r.x + wiggle / 2 + prop * (r.width - wiggle)), (int) offset, (int) ((1 - prop) * (r.width - wiggle)), (int) barHeight);

                setColor(test, game.getResourceColor(resType));
                drawScaled(c, test, filled, scaleX, scaleY);

                setColor(test, Types.BLACK);
                drawScaled(c, test, rest, scaleX, scaleY);
                offset += barHeight;
            }
        }

    }

    private void drawQuad(Canvas c, GameState game, VGDLSprite sprite, Rectangle r, double scaleX, double scaleY) {
        if (sprite.shrinkfactor != 1) {
            r.width *= sprite.shrinkfactor;
            r.height *= sprite.shrinkfactor;
            r.x += (sprite.rect.width-r.width)/2;
            r.y += (sprite.rect.height-r.height)/2;
        }

        float x = (float)(r.x * scaleX);
        float y = (float)(r.y * scaleY);
        float bx = (float)(x + (r.width * scaleX));
        float by = (float)(y + (r.height * scaleY));

        setColor(test, sprite.color);

        if (sprite.is_avatar) {
            c.drawOval(x, y, bx, by, test);
        } else {
            c.drawRect(x, y, bx, by, test);
        }
    }

    private Bitmap loadImage(String id) {
        GameState state = activity.getState();
        if (state != null) {
                try {
                    String filename = id;
                    if (!filename.endsWith(".png")) {
                        filename += ".png";
                    }

                    Bitmap bp = BitmapFactory.decodeStream(activity.getAssets().open("sprites/" + filename));
                    if (bp != null) {
                        sprites.put(id, bp);
                        return bp;
                    }
                } catch (IOException ex) {
                    System.err.println("NOPE! didn't load sprite");
                    ex.printStackTrace();
                }

        } else {
            System.err.println("DEBUG could not find state");
        }
        return null;
    }

    private void drawImage(Canvas c, GameState game, VGDLSprite sprite, Rectangle r, double scaleX, double scaleY) {
        Bitmap image = sprites.get(sprite.image);
        if (image == null) {
            image = loadImage(sprite.image);
            if (image == null) {
                drawQuad(c, game, sprite, r, scaleX, scaleY);
            }
            return;
        }

        setColor(test, sprite.color);

        if (sprite.shrinkfactor != 1) {
            r.width *= sprite.shrinkfactor;
            r.height *= sprite.shrinkfactor;
            r.x += (sprite.rect.width-r.width)/2;
            r.y += (sprite.rect.height-r.height)/2;
        }

        int x = (int)(r.x * scaleX);
        int y = (int)(r.y * scaleY);
        int bx = (int)(x + (r.width * scaleX));
        int by = (int)(y + (r.height * scaleY));

        c.drawBitmap(image, x, y, test);
        c.drawBitmap(image, null, new Rect(x,y, bx, by), test);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (controls == null) {
            return false;
        }

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Types.ACTIONS action = controls.getAction(event.getX(), event.getY());
            if (action != null) {
                GameState state = activity.getState();
                state.process(action);
                postInvalidate();
                return true;
            }
        }

        return false;
    }

    public void notifyGameOver(final boolean hasWon, double score, int ticks) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (hasWon) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(R.string.title_dialog_win);
                    builder.setMessage(R.string.title_dialog_win_text);
                    builder.setPositiveButton(R.string.button_next, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.nextLevel();
                        }
                    });
                    AlertDialog d = builder.create();
                    d.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(R.string.title_dialog_loss);
                    builder.setMessage(R.string.title_dialog_loss_text);
                    builder.setPositiveButton(R.string.button_reset, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.reset();
                        }
                    });
                    AlertDialog d = builder.create();
                    d.show();
                }
            }
        });
    }
}
