package uk.me.webpigeon.phd.gvgai.gvg.wrapper;

import android.view.View;

import core.game.Game;
import core.player.AbstractPlayer;
import uk.me.webpigeon.phd.gvgai.gvg.GameActivity;
import uk.me.webpigeon.phd.gvgai.gvg.GameState;

/**
 * Created by webpigeon on 14/02/16.
 */
public class GameRunner implements Runnable {
    private GameState state;
    private GameActivity activity;

    public GameRunner(GameActivity activity, GameState state) {
        this.activity = activity;
        this.state = state;
    }

    @Override
    public void run() {
        try {
            state.init();
            while(!state.isGameOver()){
                state.tick();
                activity.updateView();
                Thread.sleep(100);
            }

            if (state.isGameOver()) {
                activity.notifyGameOver(state.hasWon(), state.getScore(), state.getTicks());
            }
        } catch (InterruptedException ex) {

        } catch (Exception ex) {
            System.err.println("GVGAI Crashed again...");
            ex.printStackTrace();
        }
    }
}
