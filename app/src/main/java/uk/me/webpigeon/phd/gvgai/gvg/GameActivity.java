package uk.me.webpigeon.phd.gvgai.gvg;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import core.VGDLParser;
import core.game.Game;
import uk.me.webpigeon.phd.gvgai.gvg.wrapper.GVGState;
import uk.me.webpigeon.phd.gvgai.gvg.wrapper.GVGView;
import uk.me.webpigeon.phd.gvgai.gvg.wrapper.GameRunner;

public class GameActivity extends AppCompatActivity {
    public static final String GAME_CONFIG = "gameFile";
    public static final String GAME_CACHE = "gameCache";
    public static final String GAME_STATE = "gameState";
    public static final String GAME_LEVEL = "gameLevel";

    private GameState state;
    private GVGView view;
    private ProgressDialog dialog;
    private Thread gameThread;

    private GameConfig config;
    private GameCache cache;
    private int level;

    private GameStats stats;


    protected void startGame() {
        if (gameThread != null) {
            gameThread.interrupt();
        }

        GameRunner runner = new GameRunner(this, state);
        gameThread = new Thread(runner);
        gameThread.start();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = new GVGView(this);
        setContentView(view);

        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);


        if (savedInstanceState != null) {
            config = (GameConfig)savedInstanceState.getSerializable(GAME_CONFIG);
            state = (GameState)savedInstanceState.getSerializable(GAME_STATE);
            cache = (GameCache)savedInstanceState.getSerializable(GAME_CACHE);
            level = savedInstanceState.getInt(GAME_LEVEL);
            startGame();
            view.postInvalidate();
        } else {
            Intent intent = getIntent();
            config = (GameConfig)intent.getSerializableExtra(GAME_CONFIG);
            buildGame(config);
        }

        stats = new NoopStats();
    }

    @Override
    protected void onResume() {
        super.onResume();

        GameRunner runner = new GameRunner(this, state);
        gameThread = new Thread(runner);
        gameThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gameThread.interrupt();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gameThread.interrupt();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(GAME_CONFIG, config);
        outState.putSerializable(GAME_STATE, state);
        outState.putSerializable(GAME_CACHE, cache);
        outState.putSerializable(GAME_LEVEL, level);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        config = (GameConfig)savedInstanceState.getSerializable(GAME_CONFIG);
        cache = (GameCache)savedInstanceState.getSerializable(GAME_CACHE);
        state = (GameState)savedInstanceState.getSerializable(GAME_STATE);
        level = savedInstanceState.getInt(GAME_LEVEL);
    }

    private void buildGame(GameConfig config){
        dialog.show();

        GameInitialiser init = new GameInitialiser(getAssets(), this);
        init.execute(config);
    }

    public void setLevel(int i) {
        Game game = new VGDLParser().parseGame(cache.gameDef);
        game.buildStringLevel(cache.levelDefs[i]);
        this.level = i;
        setGame(game);
    }

    public void nextLevel() {
        int next = (level + 1) % cache.levelDefs.length;
        setLevel(next);
    }

    public void setGame(Game game) {
        dialog.dismiss();

        state = new GVGState(game);
        startGame();
        view.postInvalidate();
    }

    public void reset() {
        state.reset();
        setLevel(level);
        startGame();
    }

    public GameState getState() {
        return state;
    }

    public void setGameCache(GameCache gameCache) {
        this.cache = gameCache;
    }

    public void notifyGameOver(boolean hasWon, double score, int ticks) {
        if (hasWon) {
            stats.recordWin(config.gameDef, config.levelDef[level], score, ticks);
        } else {
            stats.recordLoss(config.gameDef, config.levelDef[level], score, ticks);
        }
        view.notifyGameOver(hasWon, score, ticks);
    }

    public void updateView() {
        view.postInvalidate();
    }
}
