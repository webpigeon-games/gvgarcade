package uk.me.webpigeon.phd.gvgarcade.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Game> ITEMS = new ArrayList<Game>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, Game> ITEM_MAP = new HashMap<String, Game>();

    private static final int COUNT = 25;

    private static String[] games = new String[]{"aliens", "bait", "blacksmoke", "boloadventures", "boulderchase",              //0-4
            "boulderdash", "brainman", "butterflies", "cakybaky", "camelRace",     //5-9
            "catapults", "chase", "chipschallenge", "chopper", "cookmepasta",        //10-14
            "crossfire", "defem", "defender", "digdug", "eggomania",           //15-19
            "enemycitadel", "escape", "factorymanager", "firecaster",  "firestorms",   //20-24
            "frogs", "gymkhana", "hungrybirds", "iceandfire", "infection",    //25-29
            "intersection", "jaws", "labyrinth", "lasers", "lasers2",        //30-34
            "lemmings", "missilecommand", "modality", "overload", "pacman",             //35-39
            "painter", "plants", "plaqueattack", "portals", "raceBet2",         //40-44
            "realportals", "realsokoban", "roguelike", "seaquest", "sheriff",      //45-49
            "sokoban", "solarfox" ,"superman", "surround", "survivezombies", //50-54
            "tercio", "thecitadel", "waitforbreakfast", "watergame", "whackamole", //55-59
            "zelda", "zenpuzzle" };

    static {

        for (String game : games) {
            addItem(new Game(game, game, game, game));
        }
    }

    private static void addItem(Game item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static Game createDummyItem(int position) {
        return new Game(String.valueOf(position), "Item " + position, makeDetails(position)," levelDef.txt");
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class Game {
        public final String id;
        public final String content;
        public final String details;
        public final String gameDef;
        public final String[] levelDef;

        public Game(String id, String content, String details, String gameDef) {
            this.id = id;
            this.content = content;
            this.details = details;
            this.gameDef = gameDef+".txt";
            this.levelDef = new String[5];
            for (int i=0; i<5; i++) {
                levelDef[i] = gameDef+"_lvl"+i+".txt";
            }
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
