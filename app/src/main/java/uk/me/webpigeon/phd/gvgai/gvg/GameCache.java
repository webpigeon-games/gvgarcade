package uk.me.webpigeon.phd.gvgai.gvg;

import java.io.Serializable;

/**
 * Created by webpigeon on 17/04/16.
 */
public class GameCache implements Serializable {
    public String name;
    public String[] gameDef;
    public String[][] levelDefs;
}
