package uk.me.webpigeon.phd.gvgai.gvg;

/**
 * Created by webpigeon on 17/04/16.
 */
public interface GameStats {

    void recordWin(String game, String level, double score, int ticks);
    void recordLoss(String game, String level, double score, int ticks);
}
