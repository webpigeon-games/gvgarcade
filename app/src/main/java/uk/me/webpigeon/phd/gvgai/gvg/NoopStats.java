package uk.me.webpigeon.phd.gvgai.gvg;

/**
 * Created by webpigeon on 17/04/16.
 */
public class NoopStats implements GameStats {

    @Override
    public void recordWin(String game, String level, double score, int ticks) {
        System.out.println("win: "+game+" "+level+" "+score+" "+ticks);
    }

    @Override
    public void recordLoss(String game, String level, double score, int ticks) {
        System.out.println("loss: "+game+" "+level+" "+score+" "+ticks);
    }
}
