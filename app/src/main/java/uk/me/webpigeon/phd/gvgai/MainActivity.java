package uk.me.webpigeon.phd.gvgai;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import uk.me.webpigeon.phd.R;
import uk.me.webpigeon.phd.gvgai.gvg.GameConfig;
import uk.me.webpigeon.phd.gvgai.gvg.GameActivity;
import uk.me.webpigeon.phd.gvgai.gvg.mockups.DebugActivity;
import uk.me.webpigeon.phd.gvgai.gvg.mockups.DummyGameState;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ArrayList<GameConfig> games;
    private ArrayAdapter<GameConfig> adapter;

    private static final String GAME_LIST = "TEST";
    private static final String GVGAI_WEBSITE = "http://gvgai.net";
    private static final String GAME_MANIFEST = "GameManifest.json";
    private static final String GAME_ENCODING = "UTF-8";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView list = (ListView)findViewById(R.id.GameList);
        games = new ArrayList<GameConfig>();

        adapter = new ArrayAdapter<GameConfig>(this, android.R.layout.simple_list_item_1, games);
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);

        if (savedInstanceState == null) {
            //populateGameList();
            debugGameList();
        } else {
            games = (ArrayList<GameConfig>)savedInstanceState.getSerializable(GAME_LIST);
            adapter.addAll(games);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.homepage_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.visit_gvgai) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(GVGAI_WEBSITE));
            startActivity(i);
            return true;
        }

        if (id == R.id.debug_launch) {
            Intent gameIntent = new Intent(getBaseContext(), DebugActivity.class);
            gameIntent.putExtra(GameActivity.GAME_STATE, new DummyGameState());
            startActivity(gameIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void populateGameList() {
        try {
            Gson gson = new Gson();
            Reader reader = new InputStreamReader(getAssets().open(GAME_MANIFEST), GAME_ENCODING);
            GameConfig[] gameConfigs = gson.fromJson(reader, GameConfig[].class);

            for (GameConfig config : gameConfigs) {
                adapter.add(config);
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        adapter.notifyDataSetChanged();
    }

    private static String[] DEBUG_GAMES = new String[]{"aliens", "bait", "blacksmoke", "boloadventures", "boulderchase",              //0-4
            "boulderdash", "brainman", "butterflies", "cakybaky", "camelRace",     //5-9
            "catapults", "chase", "chipschallenge", "chopper", "cookmepasta",        //10-14
            "crossfire", "defem", "defender", "digdug", "eggomania",           //15-19
            "enemycitadel", "escape", "factorymanager", "firecaster",  "firestorms",   //20-24
            "frogs", "gymkhana", "hungrybirds", "iceandfire", "infection",    //25-29
            "intersection", "jaws", "labyrinth", "lasers", "lasers2",        //30-34
            "lemmings", "missilecommand", "modality", "overload", "pacman",             //35-39
            "painter", "plants", "plaqueattack", "portals", "raceBet2",         //40-44
            "realportals", "realsokoban", "roguelike", "seaquest", "sheriff",      //45-49
            "sokoban", "solarfox" ,"superman", "surround", "survivezombies", //50-54
            "tercio", "thecitadel", "waitforbreakfast", "watergame", "whackamole", //55-59
            "zelda", "zenpuzzle" };
    protected void debugGameList() {

        for (String game : DEBUG_GAMES) {
            GameConfig config = new GameConfig(game);
            config.gameDef = "defs/"+game+".txt";
            config.levelDef = new String[5];
            for (int i=0; i<config.levelDef.length; i++) {
                config.levelDef[i] = "defs/"+game+"_lvl"+i+".txt";
            }
            adapter.add(config);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable(GAME_LIST, games);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        GameConfig item = adapter.getItem(position);

        Intent gameIntent = new Intent(getBaseContext(), GameActivity.class);
        gameIntent.putExtra(GameActivity.GAME_CONFIG, item);
        startActivity(gameIntent);
    }
}
